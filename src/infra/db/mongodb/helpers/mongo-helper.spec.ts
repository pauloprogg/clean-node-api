import { MongoHelper as sut } from './mongo-helper'

describe('Mongo Helper',() => {
  beforeAll(async () => {
    await sut.connect(process.env.MONGO_URL)
  })
  afterAll(async () => {
    await sut.disconnect()
  })
  test('should reconect if mongodb is down', async () => {
    // await sut.connect(process.env.MONGO_URL)
    // let accountCollection = sut.getCollection('account')
    // expect(accountCollection).toBeTruthy()
    // await sut.connect(process.env.MONGO_URL)
    // await sut.disconnect()
    const accountCollection2 = sut.getCollection('account')
    // await sut.disconnect()
    expect(accountCollection2).toBeTruthy()
  })
})
