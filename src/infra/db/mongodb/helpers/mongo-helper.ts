import { Collection, MongoClient } from "mongodb"
import { AccountModel } from "../../../../domain/models/account"

export const MongoHelper = {
  url: null as string,
  client: null as MongoClient,
  async connect (url: string): Promise <void> {
    this.url = url
    this.client = await MongoClient.connect(url)
  },
  async disconnect (): Promise<void> {
    this.client.close()
  },
  async getCollection (name: string): Promise<Collection> {
    if (!this.client) {
      await this.connect(this.url)
    }
    return this.client.db().collection(name)
  },
  map: (collection: any): any => {
    const { _id,...collectionWithoutId } = collection
    const accountReturn = Object.assign({},collectionWithoutId,{ id: collection._id.toJSON() })
    return accountReturn
  }
}
