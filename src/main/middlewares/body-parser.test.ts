import request from "supertest"
import app from "../config/app"

describe('body parser middlewre',() => {
  test('should parser json',async () => {
    app.post('/body_parser',(req,res) => {
      res.send(req.body)
    })
    await request(app)
      .post('/body_parser')
      .send({ name: '****' })
      .expect({ name: '****' })
  })
})
