import request from "supertest"
import app from "../config/app"

describe('CORS middlewre',() => {
  test('should enable CORS',async () => {
    app.get('/body_cors',(req,res) => {
      res.send()
    })
    await request(app)
      .post('/body_cors')
      .expect('access-control-allow-origin','*')
      .expect('access-control-allow-methods','*')
      .expect('access-control-allow-headers','*')
  })
})
