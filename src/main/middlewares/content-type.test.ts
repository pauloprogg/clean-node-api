import request from "supertest"
import app from "../config/app"

describe('Content Type middlewre',() => {
  test('should return content type as json',async () => {
    app.get('/test_contentype',(req,res) => {
      res.send()
    })
    await request(app)
      .get('/test_contentype')
      .expect('content-type',/json/)
  })
  test('should return xml content type when forced',async () => {
    app.get('/test_contentype_xml',(req,res) => {
      res.type('xml')
      res.send()
    })
    await request(app)
      .get('/test_contentype_xml')
      .expect('content-type',/xml/)
  })
})
