import { Controller, HttpRequest } from "../../presentation/protocols"
import { Request, Response } from "express"

export const adaptRoute = (controller: Controller) => {
  return async (req: Request,res: Response) => {
    const httpRequest: HttpRequest = {
      body: req.body
    }
    const httpResponse = await controller.handle(httpRequest)
    console.log(" ||||||dm sdapmdsddZZZZZs  ",httpResponse)
    res.status(httpResponse.statusCode).json(httpResponse.body)
  }
}
