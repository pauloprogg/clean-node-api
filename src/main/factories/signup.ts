import { SignUpController } from "../../presentation/controllers/signup/signup"
import { EmailValidatorAdapter } from "../../Utils/email-validator-adapter"
import { DbAddAccount } from "../../data/usecases/add-account/db-add-account"
import { BcryptAdapter } from "../../infra/criptography/bcrypt-adapter"
import { AccountMongoRepository } from "../../infra/db/mongodb/account-repository/account"

export const makeSignupController = (): SignUpController => {
  const salt = 12
  const emailValidatorAdpt = new EmailValidatorAdapter()
  const bcrytAdapter = new BcryptAdapter(salt)
  const accountMongoRepository = new AccountMongoRepository()
  const dbAddAccount = new DbAddAccount(bcrytAdapter,accountMongoRepository)
  return new SignUpController(emailValidatorAdpt,dbAddAccount)
}
