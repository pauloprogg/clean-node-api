import request from "supertest"
import app from "../config/app"
import { MongoHelper } from "../../infra/db/mongodb/helpers/mongo-helper"

describe('Signup Routes',() => {
  beforeAll(async () => {
    await MongoHelper.connect(process.env.MONGO_URL)
  })
  afterAll(async () => {
    await MongoHelper.disconnect()
  })
  beforeEach(async () => {
    const accountCollection = await MongoHelper.getCollection('accounts')
    await accountCollection.deleteMany({})
  })
  test('should return an account on success',async () => {
    await request(app)
      .post('/api/v1/signup')
      .send({
        name: 'any_name',
        email: 'any@email.com',
        password: 'any_pass',
        passwordconfirmation: 'any_pass'
      })
      .expect(200)
  })
})
