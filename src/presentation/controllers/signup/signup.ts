import { HttpResponse, HttpRequest, Controller, EmailValidator, AddAccount } from "./signup-protocols"
import { MissingParamError, InvalidParamError } from "../../errors"
import { badRequest, serverError, ok } from "../../helpers/http-helper"

export class SignUpController implements Controller {
  private readonly emailValidator: EmailValidator
  private readonly addAccount: AddAccount

  constructor (emailValidator: EmailValidator, addAccount: AddAccount) {
    this.emailValidator = emailValidator
    this.addAccount = addAccount
  }

  async handle (httpRequest: HttpRequest): Promise<HttpResponse> {
    try {
      const requiredFields = ['name', 'email','password','passwordconfirmation']
      for (const fields of requiredFields) {
        if (!httpRequest.body[fields]) {
          return badRequest(new MissingParamError(fields))
        }
      }
      const { name, email, password, passwordconfirmation } = httpRequest.body
      if (password !== passwordconfirmation) {
        return badRequest(new InvalidParamError('passworConfirmation'))
      }
      const isValid = this.emailValidator.isValid(email)
      if (!isValid) {
        return badRequest(new InvalidParamError('email'))
      }
      const account = await this.addAccount.add({
        name,
        email,
        password
      })
      return ok(account)
    } catch (error) {
      return serverError()
    }
  }
}
